import Head from 'next/head';
import Link from 'next/link';
import styles from '../styles/Home.module.css';

function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Create Next App</title>
        <link rel='icon' href='/favicon.ico' />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Bienvenido a
          {' '}
          <a href='https://nextjs.org'>Rexberry!</a>
        </h1>

        <p className={styles.description}>
          Iniciemos editando la página principal
          {' '}
          <code className={styles.code}>pages/index.js</code>
        </p>

        <div className={styles.grid}>
          <Link href='/login'>
            <a className={styles.card}>
              <h3>Login &rarr;</h3>
              <p>Iniciemos! Vamos dentro del sistema... wii!</p>
            </a>
          </Link>

          <Link href='/login'>
            <a className={styles.card}>
              <h3>Registrarse &rarr;</h3>
              <p>
                Ingresa al sistema registrándote primero
              </p>
            </a>
          </Link>
        </div>
      </main>
    </div>
  );
}

export default Home;
